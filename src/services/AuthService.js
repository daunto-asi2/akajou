import { handleFetchErrors } from '../commons/utils';

/* eslint-env browser */
/* global fetch */
// this is a mock url
const SERVER_URL = 'http://localhost:1337';
const SAVED_TOKEN = 'USER';

const saveUser = (userData) => {
  try {
    localStorage.setItem(SAVED_TOKEN, JSON.stringify(userData));
  } catch (err) {
    console.error(err);
  }
};

const removeToken = () => {
  try {
    localStorage.removeItem(SAVED_TOKEN);
  } catch (err) {
    console.error(err);
  }
};

const tokenIsNotNull = () => {
  const maybeToken = localStorage.getItem(SAVED_TOKEN);
  return maybeToken !== undefined
          && maybeToken !== null;
};

const getUsername = () => {
  try {
    const userData = JSON.parse(localStorage.getItem(SAVED_TOKEN));
    return userData.username;
  } catch (err) {
    console.error(err);
    return undefined;
  }
};

/**
 * @param {{ login: String, pwd: String }} loginAndPwd
 * @return {Promise<{ token: String }>}
 */
const login = loginAndPwd =>
  fetch(
    `${SERVER_URL}/auth`,
    {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(loginAndPwd),
    },
  )
    .then(handleFetchErrors)
    .then(data => data.json())
    .then(({ token }) => {
      saveUser({ token, username: loginAndPwd.login });
    });

const logout = removeToken;

/**
 * @return {boolean}
 */
const isAuthenticated = tokenIsNotNull;

export {
  login,
  logout,
  isAuthenticated,
  getUsername,
};
