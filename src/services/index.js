import generateUUID from './generateUUID';
import getNextSlideIndex from './getNextSlideIndex';
import getPrevSlideIndex from './getPrevSlideIndex';

export {
  generateUUID,
  getNextSlideIndex,
  getPrevSlideIndex,
};
