const getPrevSlidIndex = (array, slidId) => {
  const index = array.findIndex(a => a.id === slidId)
  return index - 1 >= 0
    ? index - 1
    : 0;
};

export default getPrevSlidIndex;
