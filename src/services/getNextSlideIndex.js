const getNextSlideIndex = (array, slidId) => {
  const index = array.findIndex(a => a.id === slidId);
  return index + 1 < array.length
    ? index + 1
    : array.length - 1;
};

export default getNextSlideIndex;
