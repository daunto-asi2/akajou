/* eslint-env browser */
/* eslint-disable no-console */

import io from 'socket.io-client';
import axios from 'axios';

class Comm {
  constructor() {
    this.comm = {};
    this.comm.io = {};
    this.socket = '';
    this.emitOnConnect = this.emitOnConnect.bind(this);
  }

  /* eslint-disable class-methods-use-this */
  toString() {
    return '';
  }

  loadPres(presId, callback, callbackErr) {
    axios.get('/loadPres')
      .then(({ data }) => {
        const dataKeys = Object.keys(data);
        const loadedPres = dataKeys.length > 0
          ? data[dataKeys[0]]
          : '';
        callback(loadedPres);
      }).catch((error) => {
        callbackErr(error);
      });
  }

  loadContent(callback, callbackErr) {
    axios.get('/resources_list')
      .then((data) => {
        console.log(data.data);
      // todo
      })
      .catch((error) => {
        callbackErr(error);
      });
  }

  savPres(presJson, callbackErr) {
    axios.post('/savePres', presJson)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        callbackErr(error);
      });
  }

  savContent(contentJson, callbackErr) {
    axios.post('/addContent', contentJson)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        callbackErr(error);
      });
  }

  fileUpload(fileC, callback, callbackErr) {
    const data = new FormData();
    data.append('file', fileC);
    axios.post('/file-upload', data)
      .then((response) => {
        console.log(response);
        callback();
      })
      .catch((error) => {
        callbackErr(error);
      });
  }
  /* eslint-enable class-methods-use-this */

  emitOnConnect(message) {
    console.log('message', message);
    console.log('socket', this.socket);
    console.log('this.comm.io.uuid', this.comm.io.uuid);
    this.socket.emit('data_comm', { id: this.comm.io.uuid }, (test) => {
      console.log(test);
    });
  }

  socketConnection(uuid) {
    this.socket = io.connect(process.env.SOCKET_URL);
    this.comm.io.uuid = uuid;

    this.socket.on('connection', (message) => {
      this.emitOnConnect(message);
    });

    this.socket.on('newPres', (socket) => {

    });

    this.socket.on('slidEvent', (socket) => {

    });
  }

  backward() {
    this.socket.emit('slidEvent', { CMD: 'PREV' });
  }

  forward() {
    this.socket.emit('slidEvent', { CMD: 'NEXT' });
  }

  play(presUUID) {
    this.socket.emit('slidEvent', { CMD: 'START', PRES_ID: presUUID });
  }

  pause() {
    this.socket.emit('slidEvent', { CMD: 'PAUSE' });
  }

  begin() {
    this.socket.emit('slidEvent', { CMD: 'BEGIN' });
  }

  end() {
    this.socket.emit('slidEvent', { CMD: 'END' });
  }
}

module.exports = Comm;
