import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import './App.css';

import {
  BrowseContentPanel,
  BrowsePresentationPanel,
  EditSlidePanel,
  NavBar,
} from './components/containers';

import { updateContentMap, updatePresentation } from './actions';
import { isAuthenticated, getUsername } from './services/AuthService';
import { ROUTES } from './commons/constants';

import * as contentMapTmp from './source/contentMap.json';
import * as presTmp from './source/pres.json';

class App extends React.Component {
  constructor(props) {
    super(props);
    props.dispatch(
      updateContentMap(contentMapTmp),
    );
    props.dispatch(
      updatePresentation(presTmp),
    );
  }

  render() {
    return !isAuthenticated()
      ? <Redirect to={ROUTES.SIGN_IN} />
      : (
        <div className="container-fluid height-100">
          <NavBar username={this.props.user.username || getUsername()} />
          <div className="row height-100">
            <div className="col-lg-3 col-md-3 height-100 vertical-scroll">
              <BrowsePresentationPanel />
            </div>
            <div className="col-lg-6 col-md-6 height-100">
              <EditSlidePanel />
            </div>
            <div className="col-lg-3 col-md-3 height-100">
              <BrowseContentPanel />
            </div>
          </div>
        </div>
      );
  }
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.shape({
    username: PropTypes.string,
    logDate: PropTypes.instanceOf(Date),
    logged: PropTypes.bool,
  }).isRequired,
};

const mapStateToProps = state => ({
  user: state.userReducer.user,
});

export default connect(mapStateToProps)(App);
