/* eslint-env browser */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';
import reducers from './reducers';

import {
  SignInContainer,
  SignUpContainer,
} from './components/containers';

import {
  AuthRoute,
  NotFound,
} from './components';

import { ROUTES } from './commons/constants';

const store = createStore(
  reducers,
);

ReactDOM.render((
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path={ROUTES.SIGN_IN} component={SignInContainer} />
        <Route path={ROUTES.SIGN_UP} component={SignUpContainer} />
        <AuthRoute path={ROUTES.PRES} component={App} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  </Provider>
), document.getElementById('root'));
