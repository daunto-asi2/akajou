const defaultSlide = {
  id: -1,
  content: -1,
  txt: 'No slide selected',
  title: 'No slide',
};

const selectedReducer = (state = { slid: defaultSlide }, action) => {
  switch (action.type) {
    case 'UPDATE_SELECTED_SLID':
      return Object.assign({}, state, { slid: action.obj || defaultSlide });
    default:
      return state;
  }
};

export default selectedReducer;
