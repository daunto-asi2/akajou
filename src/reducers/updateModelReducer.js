import { replace } from '../commons/utils';

const defaultUpdateModelState = {
  presentation: {},
  content_map: {},
};

const updateModelReducer = (state = defaultUpdateModelState, action) => {
  switch (action.type) {
    case 'UPDATE_CONTENT_MAP':
      return ({ ...state, content_map: action.content_map });

    case 'UPDATE_PRESENTATION':
      return ({ ...state, presentation: action.presentation });

    case 'UPDATE_PRESENTATION_SLID':
      return ({
        ...state,
        presentation: {
          ...state.presentation,
          slidArray:
            replace(
              state.presentation.slidArray,
              s => s.id === action.slide.id,
              action.slide,
            ),
        },
      });

    case 'UPDATE_PRESENTATION_SLIDS':
      return ({ ...state, presentation: { ...state.presentation, slidArray: action.slides } });

    case 'ADD_CONTENT':
    default:
      return state;
  }
};

export default updateModelReducer;
