
const defaultState = {
  user: {
    username: '',
    logDate: undefined,
    logged: false,
  },
};


const userReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'CONNECT_USER':
      return {
        ...state,
        user: {
          username: action.user.username,
          logDate: action.user.logDate,
          logged: true,
        },
      };

    case 'DISCONNECT_USER':
      return {
        ...state,
        ...defaultState,
      };

    default:
      return state;
  }
};

export default userReducer;
