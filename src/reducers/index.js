import { combineReducers } from 'redux';

import selectedReducer from './selectedReducer';
import updateModelReducer from './updateModelReducer';
import userReducer from './userReducer';

export default combineReducers({
  selectedReducer,
  updateModelReducer,
  userReducer,
});
