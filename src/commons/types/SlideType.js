import PropTypes from 'prop-types';

const SlideType = PropTypes.shape({
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  title: PropTypes.string.isRequired,
  txt: PropTypes.string.isRequired,
  content_id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
});

export default SlideType;
