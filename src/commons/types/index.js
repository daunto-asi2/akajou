import ContentMapType from './ContentMapType';
import SlideType from './SlideType';

export {
  ContentMapType,
  SlideType,
};
