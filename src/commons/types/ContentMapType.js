import PropTypes from 'prop-types';

const ContentMapType = PropTypes.shape({
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  title: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['img', 'img_url', 'video', 'web']).isRequired,
  src: PropTypes.string,
});

export default ContentMapType;
