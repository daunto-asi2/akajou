/**
 * @param {Response} response the HTTP Response from fetch
 * @return {Promise}
 */
const handleFetchErrors = (response) => { // eslint-disable-line arrow-body-style
  return response.ok
    ? Promise.resolve(response)
    : Promise.reject(response);
};

export default handleFetchErrors;
