/**
 * @param {any[]} array a simple array
 * @param {() => boolean} findCb a function to pass to findIndex
 * @param {any} replaceElement the element that will replace the found element in `array`
 * @see Array.findIndex
 * @return {any[]} a new array with the modified element or the original array if no element
 * returns true with `findCb`
 */
const replace = (array, findCb, replaceElement) => {
  const elementIndex = array.findIndex(findCb);
  return elementIndex !== -1
    ? [].concat(array.slice(0, elementIndex), replaceElement, array.slice(elementIndex + 1))
    : array;
};

export default replace;
