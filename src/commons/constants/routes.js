/**
 * this constant describes route names for frontend routes
 */
export default {
  SIGN_IN: '/sign_in',
  SIGN_UP: '/sign_up',
  PRES: '/pres',
};
