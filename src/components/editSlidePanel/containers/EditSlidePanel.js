import React from 'react';
import { connect } from 'react-redux';

import Slide from '../../slide/containers/Slide';
import { SlideType } from '../../../commons/types';

/* eslint-disable camelcase */
const EditSlidePanel = ({
  selected_slide,
}) => (
  <div>
    <Slide
      id={selected_slide.id}
      title={selected_slide.title}
      txt={selected_slide.txt}
      content_id={selected_slide.content_id}
      displayMode="FULL_MNG"
    />
  </div>
);
/* eslint-enable */

const mapStateToProps = state => ({
  selected_slide: state.selectedReducer.slid,
});

EditSlidePanel.propTypes = {
  selected_slide: SlideType,
};

EditSlidePanel.defaultProps = {
  selected_slide: {
    id: '0',
    title: 'No Slide Selected',
    txt: 'There is no current slide selected',
    content_id: '0',
  },
};

export default connect(mapStateToProps)(EditSlidePanel);
