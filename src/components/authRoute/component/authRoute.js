/* eslint-disable react/prop-types */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { isAuthenticated } from '../../../services/AuthService';
import { ROUTES } from '../../../commons/constants';

// inspired by http://www.thegreatcodeadventure.com/jwt-auth-with-phoenix-and-react-router-4/
const AuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => { // eslint-disable-line arrow-body-style
      return isAuthenticated()
        ? <Component {...props} />
        : <Redirect to={ROUTES.SIGN_IN} state={{ from: props.location }} />;
    }}
  />
);

export default AuthRoute;
