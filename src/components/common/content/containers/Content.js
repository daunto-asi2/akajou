import React from 'react';
import PropTypes from 'prop-types';

import { getSourceForDrag } from '../utils';

export default class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    let renderVisual;
    switch (this.props.type) {
      case 'img':
      case 'img_url':
        renderVisual = (
          <img
            draggable="true"
            onDragStart={(e) => { getSourceForDrag(e); }}
            className="imgCard"
            alt={this.props.title}
            src={this.props.src}
            contentid={this.props.id}
          />
        );
        break;
      case 'video':
      case 'web':
        renderVisual = (
          <object
            draggable="true"
            onDragStart={(e) => { getSourceForDrag(e); }}
            aria-label={this.props.title}
            data={this.props.src}
            contentid={this.props.id}
          />
        );
        break;
      default:
        break;
    }
    if (this.props.onlyContent) {
      return (
        <div className="content card">
          <h5>{this.props.title}</h5>
          {renderVisual}
        </div>
      );
    }
    return (
      <div className="content">
        {renderVisual}
      </div>
    );
  }
}

Content.propTypes = {
  type: PropTypes.oneOf(['img', 'img_url', 'video', 'web']).isRequired,
  title: PropTypes.string.isRequired,
  src: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  onlyContent: PropTypes.bool,
};

Content.defaultProps = {
  src: '',
  onlyContent: false,
};
