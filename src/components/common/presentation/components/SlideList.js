import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Slide from '../../../slide/containers/Slide';
import { SlideType } from '../../../../commons/types';

function SlideList({ slides }) {
  return (
    <div>
      {
        slides.map(slide => (
          <Slide
            key={slide.id}
            id={slide.id}
            title={slide.title}
            txt={slide.txt}
            content_id={slide.content_id}
            type={slide.type}
          />
        ))
      }
    </div>
  );
}

SlideList.propTypes = {
  slides: PropTypes.arrayOf(SlideType).isRequired,
};

const mapStateToProps = state => ({
  slides: state.updateModelReducer.presentation.slidArray,
});

export default connect(mapStateToProps)(SlideList);

