import AuthRoute from './authRoute/component/authRoute';
import ActionPresentation from './browsePresentationPanel/components/ActionPresentation';
import NotFound from './notFound/component/NotFound';
import SignIn from './sign/components/SignIn';
import SignUp from './sign/components/SignUp';
import EditMetaSlide from './slide/components/EditMetaSlide';

export {
  ActionPresentation,
  AuthRoute,
  EditMetaSlide,
  NotFound,
  SignIn,
  SignUp,
};
