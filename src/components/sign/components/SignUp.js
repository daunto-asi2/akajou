import React from 'react';
import PropTypes from 'prop-types';

const submitForm = (e, onSubmit) => {
  e.preventDefault();
  onSubmit({
    login: e.target.login.value,
    pwd: e.target.password.value,
    repeatPwd: e.target.repeatPassword.value,
  });
};

const SignUp = ({
  submitSignupForm, errors, success,
}) => (
  <div>
    <h2>Sign Up</h2>
    {
      errors.global
        ? <div className="alert alert-danger">{errors.global}</div>
        : undefined
    }
    {
      success
        ? <div className="alert alert-success">{success}</div>
        : undefined
    }
    <form className="form" onSubmit={(e) => { submitForm(e, submitSignupForm); }}>
      <div className={`form-group ${errors.login ? 'has-error' : ''}`}>
        <label htmlFor="signupForm__loginInput" className="control-label">
          Login
        </label>
        <input
          type="text"
          id="signupForm__loginInput"
          className={`form-control ${errors.login ? 'is-invalid' : ''}`}
          placeholder="john.doe"
          name="login"
          required
        />
        {
          errors.login
            ? <div className="help-block">{errors.login}</div>
            : undefined
        }
      </div>
      <div className="form-group">
        <label htmlFor="signupForm__pwdInput" className="control-labbel">
          Password
        </label>
        <input
          type="password"
          id="signupForm__pwdInput"
          className="form-control"
          name="password"
          required
        />
      </div>
      <div className={`form-group ${errors.repeatPwd ? 'has-error' : ''}`}>
        <label htmlFor="signupForm__repeatPwdInput" className="control-labbel">
          Repeat Password
        </label>
        <input
          type="password"
          id="signupForm__repeatPwdInput"
          className={`form-control ${errors.repeatPwd ? 'is-invalid' : ''}`}
          name="repeatPassword"
          required
        />
        {
          errors.repeatPwd
            ? <div className="help-block">{errors.repeatPwd}</div>
            : undefined
        }
      </div>

      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  </div>
);

SignUp.propTypes = {
  submitSignupForm: PropTypes.func.isRequired,
  errors: PropTypes.shape({
    login: PropTypes.string,
    repeatPwd: PropTypes.string,
    global: PropTypes.string,
  }),
  success: PropTypes.string,
};

SignUp.defaultProps = {
  errors: {},
  success: '',
};

export default SignUp;
