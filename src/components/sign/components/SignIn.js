import React from 'react';
import PropTypes from 'prop-types';

const submitForm = (e, onSubmit) => {
  e.preventDefault();
  onSubmit({
    login: e.target.login.value,
    pwd: e.target.password.value,
  });
};

const SignIn = ({
  submitSigninForm, error, success,
}) => (
  <div>
    <h2>Sign In</h2>
    {
      error
        ? <div className="alert alert-danger">{error}</div>
        : undefined
    }
    {
      success
        ? <div className="alert alert-success">{success}</div>
        : undefined
    }
    <form className="form" onSubmit={(e) => { submitForm(e, submitSigninForm); }}>
      <div className="form-group">
        <label htmlFor="signinForm__loginInput" className="control-label">Login</label>
        <input
          type="text"
          id="signinForm__loginInput"
          className="form-control"
          placeholder="john.doe"
          name="login"
          required
        />
      </div>
      <div className="form-group">
        <label htmlFor="signinForm__pwdInput" className="control-label">Password</label>
        <input
          type="password"
          id="signinForm__pwdInput"
          className="form-control"
          name="password"
          required
        />
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  </div>
);

SignIn.propTypes = {
  submitSigninForm: PropTypes.func.isRequired,
  error: PropTypes.string,
  success: PropTypes.string,
};

SignIn.defaultProps = {
  error: undefined,
  success: undefined,
};

export default SignIn;
