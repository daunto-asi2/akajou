import React from 'react';
import { Link, Redirect } from 'react-router-dom';

import './sign-container.css';

import SignUp from '../components/SignUp';
import { ROUTES } from '../../../commons/constants';
import { isAuthenticated } from '../../../services/AuthService';

export default class SignUpContainer extends React.Component {
  constructor() {
    super();

    this.state = {
      errors: {},
      success: '',
    };

    this.submitSignupForm = this.submitSignupForm.bind(this);
  }

  submitSignupForm({
    login,
    pwd,
    repeatPwd,
  }) {
    if (pwd.length < 6) {
      this.setState({
        errors: {
          global: 'You password should be at least 6 characters long',
        },
      });
      return;
    }

    if (repeatPwd !== pwd) {
      this.setState({
        errors: {
          global: 'Check you password repeat',
          repeatPwd: 'The repetition and password should be equals',
        },
      });
      return;
    }

    this.setState({
      success: `Welcome ${login}!`,
    });
  }

  render() {
    return (
      isAuthenticated()
        ?
          <Redirect to={ROUTES.PRES} />
        :
          <div className="container log-panel">
            <SignUp
              submitSignupForm={this.submitSignupForm}
              errors={this.state.errors}
              success={this.state.success}
            />
            <Link to={ROUTES.SIGN_IN} href={ROUTES.SIGN_IN} className="redirect-link">
              Already got an account?
            </Link>
          </div>
    );
  }
}
