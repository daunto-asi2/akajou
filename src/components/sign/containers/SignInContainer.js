import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import './sign-container.css';

import SignIn from '../components/SignIn';

import { connectUser } from '../../../actions/';
import * as AuthService from '../../../services/AuthService';
import { ROUTES } from '../../../commons/constants';

class SignInContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      success: '',
    };

    this.loginUser = props.loginUser.bind(this);

    this.submitSignInForm = this.submitSignInForm.bind(this);
  }

  submitSignInForm(loginAndPwd) {
    AuthService.login(loginAndPwd)
      .then(() => {
        this.loginUser(loginAndPwd.login);
        this.setState({
          success: `Welcome ${loginAndPwd.login}`,
        });
      }, (err) => {
        if (err instanceof TypeError) {
          this.setState({
            error: `Authentication server is unreachable,
            please check your internet connection or try again later`,
          });
          return undefined;
        }
        return err.json();
      }).then((err) => {
        if (!err) return;
        this.setState({
          error: `The server responded with the code ${err.status_code} ${err.status}
          #${err.error_code}: ${err.message}`,
        });
      })
      .catch(() => {
        this.setState({
          error: 'An error occured',
        });
      });
  }

  render() {
    return (
      AuthService.isAuthenticated()
        ?
          <Redirect to={ROUTES.PRES} />
        :
          <div className="container log-panel">
            <SignIn
              submitSigninForm={this.submitSignInForm}
              error={this.state.error}
              success={this.state.success}
            />
            <Link to={ROUTES.SIGN_UP} className="redirect-link" href={ROUTES.SIGN_UP}>
              No account yet?
            </Link>
          </div>
    );
  }
}

SignInContainer.propTypes = {
  loginUser: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  loginUser(username) { dispatch(connectUser({ username, logged: true, logDate: new Date() })); },
});

export default connect(null, mapDispatchToProps)(SignInContainer);
