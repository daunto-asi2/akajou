const getSourceFromDrop = (e) => {
  e.preventDefault();
  return e.dataTransfer.getData('contentid');
};

export default getSourceFromDrop;
