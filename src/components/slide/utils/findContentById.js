const findContentById = (contentMap, contentId) =>
  contentMap.find(content => content.id.toString() === contentId.toString());

export default findContentById;
