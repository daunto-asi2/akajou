import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Content from '../../common/content/containers/Content';
import EditMetaSlide from '../components/EditMetaSlide';
import { ContentMapType } from '../../../commons/types';
import { setSelectedSlid, updateSlide } from '../../../actions';
import { findContentById } from '../utils';

import './slide.css';

/**
 * id: id du slide
 * title: titre du slide
 * txt: texte de description du slide
 * content: id du Contenu du slide
 * contentMap: Dictionnaire des contenus disponibles issus de contentMap.json
 * displayMode: 'SHORT' ou 'FULL_MNG'
 */
class Slide extends React.Component {
  constructor(props) {
    super(props);

    this.updateTitle = this.updateTitle.bind(this);
    this.updateTxt = this.updateTxt.bind(this);
    this.updateContent = this.updateContent.bind(this);
    this.updateSelectedSlide = this.updateSelectedSlide.bind(this);
    this.updateSlideData = this.updateSlideData.bind(this);
  }

  updateSlideData(update = {}) {
    return {
      id: this.props.id,
      title: this.props.title,
      txt: this.props.txt,
      content_id: this.props.content_id,
      ...update,
    };
  }

  updateTitle(newTitle) {
    this.updateSelectedSlide({ title: newTitle });
  }

  updateTxt(newTxt) {
    this.updateSelectedSlide({ txt: newTxt });
  }

  updateContent(newContent) {
    this.updateSelectedSlide({ content_id: newContent });
  }

  updateSelectedSlide(updateObj = {}) {
    const newSlide = this.updateSlideData(updateObj);
    this.props.selectSlide(newSlide);
    this.props.updateCurrentSlide(newSlide);
  }

  render() {
    const content = this.props.content_id !== -1
      ? findContentById(this.props.contentMap, this.props.content_id)
      : undefined;
    return (
      <div >
        <div className="short-slide" onClick={this.updateSelectedSlide}>
          <h1>{this.props.title}</h1>
          <p>{this.props.txt}</p>
          {content
            ? <Content
              type={content.type}
              title={content.title}
              src={content.src}
              id={content.id.toString()}
            />
            : undefined
          }
        </div>
        {
          this.props.displayMode === 'FULL_MNG'
          ?
            <EditMetaSlide
              title={this.props.title}
              txt={this.props.txt}
              src={this.props.src}
              handleChangeTitle={this.updateTitle}
              handleChangeTxt={this.updateTxt}
              handleChangeContent={this.updateContent}
            />
          : undefined
        }
      </div>
    );
  }
}

Slide.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  title: PropTypes.string.isRequired,
  txt: PropTypes.string.isRequired,
  content_id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  src: PropTypes.string,
  displayMode: PropTypes.oneOf(['SHORT', 'FULL_MNG']),
  contentMap: PropTypes.arrayOf(ContentMapType),
  selectSlide: PropTypes.func.isRequired,
  updateCurrentSlide: PropTypes.func.isRequired,
};

Slide.defaultProps = {
  displayMode: 'SHORT',
  contentMap: [],
  content_id: -1,
  src: '',
};

const mapStateToProps = state => ({
  contentMap: state.updateModelReducer.content_map,
});

const mapDispatchToProps = dispatch => ({
  selectSlide(slide) { dispatch(setSelectedSlid(slide)); },
  updateCurrentSlide(slide) { dispatch(updateSlide(slide)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Slide);
