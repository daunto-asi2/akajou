import React from 'react';
import PropTypes from 'prop-types';

import './editMetaSlide.css';
import { getSourceFromDrop, allowDrop } from '../utils/';

const EditMetaSlide = ({
  title, txt, src, handleChangeTitle, handleChangeTxt, handleChangeContent,
}) => (
  <div className="form-group">
    <div>
      <label htmlFor="currentSlideTitle">Title &nbsp;</label>
      <input
        type="text"
        className="formControl"
        id="currentSlideTitle"
        value={title}
        onChange={(e) => { handleChangeTitle(e.target.value); }}
      />
    </div>
    <div>
      <label htmlFor="currentSlideText">Text</label>
      <textarea
        id="currentSlideText"
        cols="30"
        rows="5"
        className="form-control"
        type="text"
        value={txt}
        onChange={(e) => { handleChangeTxt(e.target.value); }}
      />
    </div>
    <div>
      <label htmlFor="currentSlideImage">image</label>
      <input
        className="dropspace"
        onDragOver={(e) => { allowDrop(e); }}
        onDrop={(e) => { handleChangeContent(getSourceFromDrop(e)); }}
        type="image"
        src={src}
        alt="drop image here"
      />
    </div>
  </div>
);

EditMetaSlide.propTypes = {
  title: PropTypes.string.isRequired,
  txt: PropTypes.string,
  src: PropTypes.string,
  handleChangeTitle: PropTypes.func.isRequired,
  handleChangeTxt: PropTypes.func.isRequired,
  handleChangeContent: PropTypes.func.isRequired,
};

EditMetaSlide.defaultProps = {
  txt: '',
  src: '',
};

export default EditMetaSlide;
