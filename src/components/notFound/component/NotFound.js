import React from 'react';
import { Redirect } from 'react-router-dom';

import { isAuthenticated } from '../../../services/AuthService';

const NotFound = () => (
  <Redirect to={
    isAuthenticated()
      ? '/sign_up'
      : '/pres'
  }
  />
);

export default NotFound;
