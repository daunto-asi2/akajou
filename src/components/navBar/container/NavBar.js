import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { disconnectUser } from '../../../actions';

import { logout } from '../../../services/AuthService';
import { ROUTES } from '../../../commons/constants';

const NavBar = ({
  username, disconnect,
}) => (
  <nav className="navbar navbar-inverse">
    <div className="container">
      <ul className="nav navbar-nav navbar-right">
        <li className="navbar-text">{username}</li>
        <li><a href={ROUTES.SIGN_IN} onClick={disconnect}>disconnect</a></li>
      </ul>
    </div>
  </nav>
);

NavBar.propTypes = {
  username: PropTypes.string.isRequired,
  disconnect: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  disconnect() {
    logout();
    dispatch(disconnectUser());
  },
});

export default connect(null, mapDispatchToProps)(NavBar);
