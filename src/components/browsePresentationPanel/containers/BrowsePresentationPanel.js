import React from 'react';

import Presentation from '../../common/presentation/containers/Presentation';

const BrowsePresentationPanel = () => (
  <Presentation />
);

export default BrowsePresentationPanel;
