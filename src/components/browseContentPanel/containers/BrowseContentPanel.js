import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Content from '../../common/content/containers/Content';
import { ContentMapType } from '../../../commons/types';

function BrowseContentPanel({ contentMap }) {
  return (
    <div className="ContentPanel">
      {
        contentMap.map(element => (
          <Content
            key={element.id}
            src={element.src}
            title={element.title}
            type={element.type}
            id={element.id}
          />
        ))
      }
    </div>
  );
}

BrowseContentPanel.propTypes = {
  contentMap: PropTypes.arrayOf(ContentMapType).isRequired,
};

const mapStateToProps = state => ({
  contentMap: state.updateModelReducer.content_map,
});

export default connect(mapStateToProps)(BrowseContentPanel);
