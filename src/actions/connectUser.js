const connectUser = user => ({
  user,
  type: 'CONNECT_USER',
});

export default connectUser;
