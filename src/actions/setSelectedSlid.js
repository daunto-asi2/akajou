const setSelectedSlid = slide => ({
  type: 'UPDATE_SELECTED_SLID',
  obj: slide,
});

export default setSelectedSlid;
