const disconnectUser = user => ({
  user,
  type: 'DISCONNECT_USER',
});

export default disconnectUser;
