const updatePresentation = presentation => ({
  type: 'UPDATE_PRESENTATION',
  presentation,
});

export default updatePresentation;
