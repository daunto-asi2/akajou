const updateContentMap = contentMap => ({
  type: 'UPDATE_CONTENT_MAP',
  content_map: contentMap,
});

export default updateContentMap;
