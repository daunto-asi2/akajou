const updateSlide = slide => ({
  type: 'UPDATE_PRESENTATION_SLID',
  slide,
});

export default updateSlide;
